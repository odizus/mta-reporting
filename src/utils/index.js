import nanoajax from 'nanoajax';

/**
 * @constructor
 */
export class BigNumber {
	/**
	 * @param {number} value
	 * @param {Object} options
	 */
	constructor(value, options = {nbsp: true}) {
		this.value = value;
		this.options = options;

		this.update(value);
	}
	update(value) {
		this.value = value;

		if(value >= 1000 && value < 1000000) {
			this.number = Math.floor(value / 1000);
			this.scale = 'тыс';
		} else if(value >= 1000000 && value < 1000000000) {
			this.number = Math.floor(value / 100000) / 10 ;
			this.scale = 'млн';
		} else if (value >= 1000000000) {
			this.number = Math.floor(value / 100000000) / 10;
			this.scale = 'млрд';
		} else {
			this.number = value;
			this.scale = '';
		}
	}
	toString () {
		let space = this.options.nbsp ? '\xa0' : ' ';
		return this.number + space + ( this.scale || '');
	}
}

const loadData = (url, getQuery) => (
	new Promise((resolve, reject) => {
		let queryObject = {
			currentFromDate: getQuery.currentFromDate,
			currentToDate: getQuery.currentToDate,
			prevFromDate: getQuery.prevFromDate,
			prevToDate: getQuery.prevToDate,
			companyGroup: getQuery.companyGroup,
			unit: getQuery.unit
		};

		/**
		 * Generate get request query string
		 *
		 * @type {string}
		 */
		let query = Object.keys(queryObject).map(key => {
			return key + '=' + queryObject[key];
		}).join('&');

		nanoajax.ajax({ url: url + '?' + query }, (code, res) => {
			if(code !== 200) reject('Request error');

			if (JSON.parse(res).length) {
				resolve(JSON.parse(res));
			} else {
				reject('Empty response');
			}
		});
	})
);

export default BigNumber;
export { loadData };

