import { loadData } from '../utils';

export function changePeriod(period) {
	return {
		type: 'CHANGE_PERIOD',
		payload: period
	};
}

export function toggleSidebar() {
	return {
		type: 'TOGGLE_SIDEBAR'
	};
}

export function changeTitle(title) {
	return {
		type: 'CHANGE_TITLE',
		title: title
	};
}

export function getEntities(response) {
	response = response[0];
	return {
		type: 'GET_ENTITIES',
		data: response || {}
	};
}

export function loadEntities(query) {
	return dispatch => {
		return loadData('/api/entities', query)
		.then(response => dispatch(getEntities(response)));
	};
}

export function getCompanies(response) {
	return {
		type: 'GET_COMPANIES',
		companies: response
	};
}

export function loadCompanies(period) {
	return dispatch => {
		return loadData('/api/companies', period)
			.then(response => dispatch(getCompanies(response)));
	};
}

export function changeCompanyGroup(group) {
	return {
		type: 'CHANGE_COMPANY_GROUP',
		group
	};
}
