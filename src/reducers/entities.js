const initialState = {
	entities: [],
	paymentMethods: [],
	transactionsTotal: 0,
	transactionsTotalDelta: 0,
	transactionsCash: 0,
	transactionsCashDelta: 0,
	transactionsCashPercentage: 0,
	transactionsCashPercentageDelta: 0,
	transactionsNonCash: 0,
	transactionsNonCashDelta: 0,
	transactionsNonCashPercentage: 0,
	transactionsNonCashPercentageDelta: 0,
	revenueTotal: 0,
	revenueTotalDelta: 0,
	revenueCash: 0,
	revenueCashDelta: 0,
	revenueCashPercentage: 0,
	revenueCashPercentageDelta: 0,
	revenueNonCash: 0,
	revenueNonCashDelta: 0,
	revenueNonCashPercentage: 0,
	revenueNonCashPercentageDelta: 0,
	averageCostTotal: 0,
	averageCostCash: 0,
	averageCostNonCash: 0,
	averageCostTotalDelta: 0,
	averageCostCashDelta: 0,
	averageCostNonCashDelta: 0
};

const entities = (state = initialState, action) => {
	switch (action.type) {
		case 'GET_ENTITIES':
			return {
				...state,
				...action.data
			};
		default:
			return state;
	}
};

export default entities;
