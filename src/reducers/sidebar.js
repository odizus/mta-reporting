const initialState = {
	isActive: false
};

const sidebar = (state = initialState, action) => {
	switch (action.type) {
		case 'TOGGLE_SIDEBAR':
			return {
				...state,
				isActive: !state.isActive
			};
		default:
			return state;
	}
};

export default sidebar;
