import moment from 'moment';

import intervals from '../intervals.js';

const initialState = {
	currentFromDate: intervals.yesterday.currentFromDate,
	currentToDate: intervals.yesterday.currentToDate,
	prevFromDate: intervals.yesterday.prevFromDate,
	prevToDate: intervals.yesterday.prevToDate,
	unit: 'day',
	active: 'yesterday'
};

export default function period(state = initialState, action) {
	switch (action.type) {
		case 'CHANGE_PERIOD':
			return {
				...state,
				currentFromDate: action.payload.currentFromDate,
				currentToDate: action.payload.currentToDate,
				prevFromDate: action.payload.prevFromDate,
				prevToDate: action.payload.prevToDate,
				unit: action.payload.unit,
				active: action.payload.active
			};
		default:
			return state;
	}
}
