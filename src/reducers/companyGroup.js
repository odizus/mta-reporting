const initialState = {
	companyGroup: ''
};

const companyGroup = (state = initialState, action) => {
	switch (action.type) {
		case 'CHANGE_COMPANY_GROUP':
			return {
				...state,
				companyGroup: action.group
			};
		default:
			return state;
	}
};

export default companyGroup;
