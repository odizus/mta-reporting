import * as React from 'react';
import * as ReactDOM from 'react-dom';
import AppContainer from './containers/AppContainer';
import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import persistState from 'redux-localstorage';
import thunkMiddleware from 'redux-thunk';
import { Provider } from 'react-redux';
import * as reducers from './reducers';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import { syncHistoryWithStore, routerReducer as routing } from 'react-router-redux';
import DashboardContainer from './containers/DashboardContainer';
import CompaniesContainer from './containers/CompaniesContainer';
import Refills from './components/Refills';
const moment = require('moment');
import './styles';

moment.locale('ru');

const createPersistentStore = compose(
    persistState(),
	applyMiddleware(thunkMiddleware),
	window.devToolsExtension ? window.devToolsExtension() : f => f,

)(createStore);

const store = createPersistentStore(
    combineReducers({
        ...reducers,
        routing: routing
    }),
    {}

);

const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
	<Provider store={store}>
		<Router history={history}>
			<Route path="/" component={AppContainer}>
				<IndexRoute component={DashboardContainer} title="Общие показатели" />
				<Route path='companies' component={CompaniesContainer} title="Рейтинг перевозчиков" />
				<Route path='refills' component={Refills} title="Продажи и пополнения ЕТК" />
			</Route>
		</Router>
	</Provider>,
	document.getElementById('root')
);
