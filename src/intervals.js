import moment from 'moment';

export default {
	yesterday: {
		label: 'Вчера',
		currentFromDate: moment().startOf('day').subtract(1, 'days').valueOf(),
		currentToDate: moment().startOf('day').valueOf(),
		prevFromDate: moment().startOf('day').subtract(2, 'days').valueOf(),
		prevToDate: moment().startOf('day').subtract(1, 'days').valueOf(),
		unit: 'day',
		active: 'yesterday'
	},
	week: {
		label: 'Неделя',
		currentFromDate: ( moment().isSame( moment().startOf('week') ) ) ? moment().subtract(1, 'weeks').startOf('week').valueOf() : moment().subtract(1, 'weeks').valueOf(),
		currentToDate: moment().startOf('day').valueOf(),
		prevFromDate: ( moment().isSame( moment().startOf('week') ) ) ? moment().subtract(2, 'weeks').startOf('week').valueOf() : moment().subtract(2, 'weeks').valueOf(),
		prevToDate: moment().startOf('day').subtract(1, 'weeks').valueOf(),
		unit: 'day',
		active: 'week'
	},
	month: {
		label: 'Месяц',
		currentFromDate: ( moment().isSame( moment().startOf('month') ) ) ? moment().subtract(1, 'months').startOf('month').valueOf() : moment().subtract(1, 'months').valueOf(),
		currentToDate: moment().startOf('day').valueOf(),
		prevFromDate: ( moment().isSame( moment().startOf('month') ) ) ? moment().subtract(2, 'months').startOf('month').valueOf() : moment().subtract(2, 'months').valueOf(),
		prevToDate: moment().startOf('day').subtract(1, 'months').valueOf(),
		'unit': 'day',
		active: 'month'
	},
	quarter: {
		label: 'Квартал',
		currentFromDate: ( moment().isSame( moment().startOf('quarter') ) ) ? moment().subtract(1, 'quarters').startOf('quarter').valueOf() : moment().subtract(1, 'quarters').valueOf(),
		currentToDate: moment().startOf('day').valueOf(),
		prevFromDate: ( moment().isSame( moment().startOf('quarter') ) ) ? moment().subtract(2, 'quarters').startOf('quarter').valueOf() : moment().subtract(2, 'quarters').valueOf(),
		prevToDate: moment().startOf('day').subtract(1, 'quarters').valueOf(),
		'unit': 'month',
		active: 'quarter'
	},
	year: {
		label: 'Год',
		currentFromDate: ( moment().isSame( moment().startOf('year') ) ) ? moment().subtract(1, 'years').startOf('year').valueOf() : moment().subtract(1, 'years').valueOf(),
		currentToDate: moment().startOf('day').valueOf(),
		prevFromDate: ( moment().isSame( moment().startOf('year') ) ) ? moment().subtract(2, 'years').startOf('year').valueOf() : moment().subtract(2, 'years').valueOf(),
		prevToDate: moment().startOf('day').subtract(1, 'years').valueOf(),
		'unit': 'month',
		active: 'year'
	},
	all: {
		label: 'С начала',
		currentFromDate: moment('2015-02-01'),
		currentToDate: moment().startOf('day').valueOf(),
		prevFromDate: moment('2015-02-01'),
		prevToDate: moment().startOf('day').valueOf(),
		'unit': 'month',
		active: 'all'
	},
};
