import React, { Component } from 'react';
import Dashboard from '../components/Dashboard/Dashboard';
import { connect } from 'react-redux';
import { loadEntities, changeTitle } from '../actions';

@connect(state => ({
	period: state.period,
	companyGroup: state.companyGroup.companyGroup,
	entities: state.entities
}))
class DashboardContainer extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		let { period, companyGroup, dispatch } = this.props;

		dispatch( changeTitle('Основные показатели') );
		dispatch( loadEntities({
			...period,
			...{ companyGroup: companyGroup }
		}) );
	}

	componentWillUpdate(nextProps) {
		let { period, companyGroup } = nextProps;

		if(period.active !== this.props.period.active || companyGroup !== this.props.companyGroup) {
			nextProps.dispatch(loadEntities({
				...period,
				...{ companyGroup: companyGroup }
			}));
		}

		return nextProps;
	}

	render() {
		let { entities } = this.props;
		return (
			<Dashboard data={entities} />
		);
	};
}

export default DashboardContainer;
