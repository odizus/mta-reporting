import React, { Component } from 'react';
import Companies from '../components/Companies/Companies';
import { connect } from 'react-redux';
import { loadCompanies, changeTitle } from '../actions';

@connect(state => ({
	period: state.period,
	companies: state.companies
}))
class CompaniesContainer extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		let { period, dispatch } = this.props;

		dispatch(changeTitle('Рейтинг перевозчиков'));
		dispatch(loadCompanies(period));
	}

	componentWillUpdate(nextProps) {
		let { period } = nextProps;

		if(period.active !== this.props.period.active) {
			nextProps.dispatch(loadCompanies(period));
		}

		return nextProps;
	}

	render() {
		let { companies } = this.props;

		companies = Object.keys(companies).map(idx => companies[idx]);

		return (
			<Companies companies={companies} />
		);
	};
};

CompaniesContainer.defaultProps = {
	companies: []
};

export default CompaniesContainer;
