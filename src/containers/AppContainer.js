import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import App from '../components/App/App';
import Page from '../components/Page/Page';
import Sidebar from '../components/Sidebar/Sidebar';
import Header from '../components/Header/Header';
import * as Actions from '../actions';

/**
 * App container
 */
@connect( state => ({
	isSidebarActive: state.sidebar.isActive,
	period: state.period,
	title: state.app.title
}))
class AppContainer extends Component {
	constructor(props) {
		super(props);
	}

	static propTypes = {
		children: PropTypes.oneOfType([
			PropTypes.arrayOf(PropTypes.node),
			PropTypes.node
		]),
		isSidebarActive: PropTypes.bool
	};

	render() {
		let { children, isSidebarActive, dispatch, title } = this.props;
		let { toggleSidebar } = bindActionCreators(Actions, dispatch);

		return (
			<App>
				<Header title={title} toggleSidebar={toggleSidebar} />
				<Page>
					<Sidebar isActive={isSidebarActive} />
					{ children }
				</Page>
			</App>
		);
	}
}

export default AppContainer;
