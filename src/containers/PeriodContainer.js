import React from 'react';
import Period from '../components/Period/Period';
import { connect } from 'react-redux';
import * as Actions from '../actions';
import { bindActionCreators } from 'redux';

@connect(state => ({
	period: state.period
}))
class PeriodContainer extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		let { period, dispatch } = this.props;
		let { changePeriod } = bindActionCreators(Actions, dispatch);


		return (
			<Period period={period} onChange={changePeriod} />
		);
	};
}

export default PeriodContainer;
