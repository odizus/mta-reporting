import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionCreators from '../actions';
import ControlGroup from '../components/ControlGroup/ControlGroup';
import Button from '../components/Button/Button';

@connect( state => ({
	companyGroup: state.companyGroup.companyGroup
}) )
export default class CompanyGroupContainer extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let { dispatch, companyGroup } = this.props;
		let { changeCompanyGroup } = bindActionCreators(ActionCreators, dispatch);
		return (
			<ControlGroup isVertical={true}>
				<Button onClick={() => changeCompanyGroup('')} active={!companyGroup}>Все</Button>
				<Button onClick={() => changeCompanyGroup('ГУП МО "МОСТРАНСАВТО"')} active={companyGroup === 'ГУП МО "МОСТРАНСАВТО"'}>ГУП МО «МОСТРАНСАВТО»</Button>
				<Button onClick={() => changeCompanyGroup('Прочие перевозчики')} active={companyGroup === 'Прочие перевозчики'}>Прочие перевозчики</Button>
			</ControlGroup>
		);
	}
}
