import React, { PropTypes } from 'react';
import CSSModules from 'react-css-modules';
import * as styles from './app.scss';

const App = ({ children }) =>
	<div>
		{children}
	</div>
;

App.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(
			PropTypes.node
		),
		PropTypes.node
	])
};

export default CSSModules(App, styles, {allowMultiple: true});
