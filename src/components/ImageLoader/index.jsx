import React  from 'react';
import * as style  from './style';

class ImageLoader extends React.Component {
	render() {
		return (
			<div className={this.props.className + ' ' + style.control } >
				<label htmlFor={this.props.name} className={style.label}>{this.props.label}</label>
				<input className={style.text} type="file" name={this.props.name} id={this.props.name} />
			</div>
		);
	}
}

ImageLoader.propTypes = {
	className: React.PropTypes.string,
	name: React.PropTypes.string,
	label: React.PropTypes.string
};

export default ImageLoader;
