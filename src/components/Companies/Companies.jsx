import React, { PropTypes } from 'react';
import { Table, Widget, DataIndex } from 'components/tepdesk';
import CSSModules from 'react-css-modules';
import * as styles from './companies.scss';


const Companies = ({ companies }) => {

	/**
	 * @FIX: Last item is array
	 */
	companies.pop();

	let data = {
		header: [
			'Наименование предприятия',
			'Количество маршрутов',
			'Количество терминалов на линии',
			'Выручка, руб',
			'Количество транзакций',
			'Количество транзакций за наличные средства',
			'Количество транзакций по безналичному способу',
			'Транзакций на терминал'
		],
		rows: companies.map(company => ({
			data: [
				company.label,
				company.routes,
				company.terminals,
				company.revenue,
				company.transactions,
				company.transactionsCash,
				company.transactionsNoncash,
				company.transactionsAvg / company.terminals
			],
			classes: [
				styles.company,
				( (company.transactionsAvg/company.terminals) >= 50 ) ? styles.companyGreen : ( (company.transactionsAvg/company.terminals) < 10 ) ? styles.companyRed : styles.companyYellow
			]
		}) )
	};

	let greenCompanies = companies.filter( company => {
		let transactionsPerTerminal = company.transactionsAvg / company.terminals;
		return ( transactionsPerTerminal >= 50 );
	}).length;

	let yellowCompanies = companies.filter(company => {
		let transactionsPerTerminal = company.transactionsAvg / company.terminals;
		return ( transactionsPerTerminal >= 10 && transactionsPerTerminal < 50 );
	}).length;

	let redCompanies = companies.filter(company => {
		let transactionsPerTerminal = company.transactionsAvg / company.terminals;
		return (transactionsPerTerminal < 10);
	}).length;

	return (
		<div className="content">
			<Widget col={4}>
				<DataIndex
					label="Перевозчиков"
					value={companies.length}
					text="в рейтинге"
					/>
				<DataIndex
					label="Перевозчиков"
					value={greenCompanies}
					text="в «зелёной» зоне"
					/>
				<DataIndex
					label="Перевозчиков"
					value={yellowCompanies}
					text="в «жёлтой» зоне"
					/>
				<DataIndex
					label="Перевозчиков"
					value={redCompanies}
					text="в «красной» зоне"
					/>
			</Widget>
			{ companies.length ? <Table data={data} /> : 'Нет данных' }
		</div>
	);
};

Companies.defaultProps = {
	companies: []
};

Companies.propTypes = {
	companies: PropTypes.array
};

export default CSSModules(Companies, styles, {allowMultiple: true});
