import classnames  from 'classnames';
import React  from 'react';
import InlineSVG  from 'svg-inline-react';
import * as style  from './style';

const Icon = ({image, className}) => (
	<InlineSVG
		src={require('../../svg/' + image + '.svg')}
		element="span"
		className={classnames(style.icon, className)}
	/>
);

Icon.displayName = 'Icon';

export default Icon;
