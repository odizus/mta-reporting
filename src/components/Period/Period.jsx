import React from 'react';
import Button from '../Button/Button';
import moment from 'moment';
import intervals from '../../intervals.js';
import * as styles from './period.scss';
import CSSModules from 'react-css-modules';

const Period = ({ period, onChange, className }) => (
	<div className={className} styleName="period">
		{ Object.keys(intervals).map( (interval, key) =>
			<Button className="button"
			        styleName="button"
					active={ intervals[interval].active === period.active }
					key={key}
					onClick={ () => { onChange(intervals[interval]); } }
					title={moment(intervals[interval].currentFromDate).format('DD.MM.YYYY') + ' — ' + moment(intervals[interval].currentToDate).format('DD.MM.YYYY')}
			>
				{intervals[interval].label}
			</Button>
		)}
	</div>
);

export default CSSModules(Period, styles, { allowMultiple: true });
