import * as React from 'react';
import InlineSVG from 'svg-inline-react';
import classnames from 'classnames';
import * as style from './style';

const Svg = ({image, className}) => (
	<InlineSVG
		src={require('../../svg/' + image + '.svg')}
		element="span"
		className={classnames(style.svg, className)}
	/>
);

export default Svg;
