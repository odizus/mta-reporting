import * as React from 'react';
import classnames from 'classnames';
import * as styles from './sidebar.scss';
import PeriodContainer from '../../containers/PeriodContainer';
import CompanyGroupContainer from 'containers/CompanyGroupContainer';
import CSSModules from 'react-css-modules';

const Sidebar = ({ isActive }) => (
	<aside styleName={ classnames('sidebar', {'sidebar--active': isActive}) }>
		<div styleName="section">
			<div styleName="section__title">Период</div>
			<PeriodContainer />
		</div>

		<div styleName="section">
			<div styleName="section__title">Группа компаний</div>
			<CompanyGroupContainer />
		</div>

	</aside>
);

export default CSSModules(Sidebar, styles, {allowMultiple: true});
