import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import * as styles from './controlGroup.scss';

const ControlGroup = ({ children, isVertical, className }) => (
	<div styleName="group group--vertical" >
		{ children.map(item => ({...item, ...{ props: {
				...item.props,
				className: [ styles.children, item.props.className].join(' ')
			} }
		}) ) }
	</div>
);

ControlGroup.defaultProps = {
	isVertical: true
};

export default CSSModules(ControlGroup, styles, { allowMultiple: true});
