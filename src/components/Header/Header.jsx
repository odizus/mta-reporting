import React, { PropTypes } from 'react';
import CSSModules from 'react-css-modules';
import Button from 'components/Button/Button';
import Svg from 'components/Svg';
import { Link, IndexLink } from 'react-router';
import * as styles  from './header.scss';

/**
 * Header React component
 */
const Header = ({ className, title, children, toggleSidebar }) =>
	<header styleName="header" className={className}>
		<Button icon="menu" styleName="sidebar-toggle" onClick={toggleSidebar} />
		<Svg image="logo" styleName="logo" />
		<Svg image="chevron" styleName="arrow" />
		<div styleName="breadcrumbs">
			{title}
		</div>
		{children}
		<nav styleName="menu">
			<IndexLink to="/" styleName="menuitem" activeClassName={styles['menuitem--active']}>Общие показатели</IndexLink>
			<Link to="companies" styleName="menuitem" activeClassName={styles['menuitem--active']}>Рейтинг перевозчиков</Link>
			<Link to="refills" styleName="menuitem" activeClassName={styles['menuitem--active']}>Продажи и пополнения ЕТК</Link>
		</nav>
	</header>
;

Header.propTypes = {
	className: PropTypes.string,
	title: PropTypes.string,
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(
			PropTypes.node
		),
		PropTypes.node
	]),
	toggleSidebar: PropTypes.func
};

export default CSSModules(Header, styles, {allowMultiple: true});
