import classnames from 'classnames';
import * as React from 'react';
import InlineSVG  from 'svg-inline-react';
import CSSModules from 'react-css-modules';
import * as styles  from './trend.scss';

const Trend = ({ className, trend, isTrendReversed }) => (
	( trend !== 0 && !isNaN(trend) ) &&
	<InlineSVG
		src={require('../../../svg/trend.svg')}
		element="span"
		className={className}
		styleName={ classnames({
			'trend': true,
			'trend--down': trend < 0,
			'trend--reversed': isTrendReversed
		}) }
	/>
);

Trend.propTypes = {
	className: React.PropTypes.string,
	trend: React.PropTypes.number,
	isTrendReversed: React.PropTypes.bool
};

Trend.defaultProps = {
	className: '',
	trend: 0,
	isTrendReversed: false
};


export default CSSModules(Trend, styles, { allowMultiple: true });
