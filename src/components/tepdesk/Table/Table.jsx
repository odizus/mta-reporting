import React, { Component } from 'react';
import * as styles from './table.scss';
import CSSModules from 'react-css-modules';
import classnames from 'classnames';

@CSSModules(styles, { allowMultiple: true })
export default class Table extends Component {
	constructor(props) {
		super(props);

		this.state = {
			headerColsWidth: []
		};

		this.header = [];
		this.fixedHeader = [];
	}

	resizeHeader() {
		this.fixedHeader.forEach( (col, idx) => { col.width = this.header[idx].offsetWidth; } );
	}

	componentDidMount() {
		this.resizeHeader();
		tableframe.addEventListener('resize', () => this.resizeHeader());
	}


	render() {
		let { header, rows } = this.props.data;

		return (
			<div styleName="wrapper">
				<table styleName="fixed-table">
					<thead>
						<tr>
							{ header.map( (column, key) => (
								 <th styleName="header__cell--fixed" key={key} ref={ c => this.fixedHeader[key] = c}>
									 {column}
								 </th>
							 ) ) }
						</tr>
					</thead>
				</table>
				<div styleName="content">
					<table styleName="table">
						<thead>
							<tr>
								{ header.map( (column, key) => (
									<th styleName="header__cell" key={key} ref={ c => this.header[key] = c}>
										{column}
									</th>
								) ) }
							</tr>
						</thead>
						<tbody>
							{ rows.map( (row, key) => (
								<tr key={key} className={classnames(row.classes)} >
									{ row.data.map( (column, key) => <td key={key} styleName="cell" ref={ col => this.header[key] = col }>{ column }</td> ) }
								</tr>
							) ) }
						</tbody>
					</table>
					<iframe name="tableframe" width="100%" height="100%" styleName="table-iframe"></iframe>
				</div>
			</div>
		);
	}
}

Table.propTypes = {
	data: React.PropTypes.object
};

Table.defaultProps = {
	data: []
};
