import * as React from 'react';
import Trend from '../Trend/Trend';
import { BigNumber } from 'utils/index.js';
import * as styles from './dataindex.scss';
import CSSModules from 'react-css-modules';

const DataIndex = ({ value, label, text, trend, isTrendReversed, className }) => (
	<div className={className} styleName="index">
		<div styleName="content">
			<div styleName="value">
				<span styleName="value__number">{ new BigNumber(value).number }</span>&nbsp;
				<span styleName="value__scale">{ new BigNumber(value).scale }</span>
			</div>
			<Trend trend={trend} isTrendReversed={isTrendReversed}/>
		</div>
		{ label && <div styleName="line"></div> }
		<div styleName="footer">
			{ label && <div styleName="label">{label}</div> }
			{ text && <div styleName="text">{text}</div> }
		</div>
	</div>
);

DataIndex.propTypes = {
	className: React.PropTypes.string,
	label: React.PropTypes.string,
	text: React.PropTypes.string,
	popup: React.PropTypes.string,
	trend: React.PropTypes.number,
	negative: React.PropTypes.bool
};

DataIndex.defaultProps = {
	className: '',
	value: 0,
	label: '',
	text: ''
};

export default CSSModules(DataIndex, styles);
