import React, { PropTypes } from 'react';
import * as styles from './widget.scss';
import CSSModules from 'react-css-modules';

const Widget = ({ children, title, className }) => (
	<div className={className} styleName="widget">
		{title && (
			<div styleName="header">
				<div styleName="title">
					{title}
				</div>
			</div>
		)}
		<div styleName="content">
			{children}
		</div>
	</div>
);

Widget.defaultProps = {
	className: '',
	title: ''
};

Widget.propTypes = {
	className: PropTypes.string,
	title: PropTypes.string
};

export default CSSModules(Widget, styles, { allowMultiple: true });
