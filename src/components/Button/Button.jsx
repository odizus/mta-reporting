import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import Icon from 'components/Icon';
import classnames from 'classnames';
import CSSModules from 'react-css-modules';
import * as styles from './button.scss';

const Button = ({ children, className, icon, type, to, active, title, onClick }) => (
	to ?
		<Link
			className={className}
			to={to}
			active={active}
			title={title}
		>
			{ icon && <Icon image={icon} styleName="icon" /> }
			{children}
		</Link>
	:
		<button
			className={className}
			styleName={ classnames('button', {'button--active': active}) }
			type={type}
			onClick={ onClick && onClick.bind(this) }
			active={active}
			title={title}
		>
			{ icon && <Icon image={icon} styleName="icon" /> }
			{children}
		</button>
);

Button.defaultProps = {
	children: null,
	to: '',
	className: '',
	type: 'button',
	icon: '',
	active: false
};

Button.propTypes = {
	to: PropTypes.string,
	className: PropTypes.string,
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node
	]),
	type: PropTypes.string,
	icon: PropTypes.string,
	onClick: PropTypes.func,
	active: PropTypes.bool
};

export default CSSModules(Button, styles, {allowMultiple: true});
