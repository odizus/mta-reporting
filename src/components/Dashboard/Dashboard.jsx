import React from 'react';
import { DataIndex, Widget } from 'tepdesk';
const Chart = require('react-google-charts').Chart;
import * as styles from './dashboard.scss';
import CSSModules from 'react-css-modules';
import moment from 'moment';


const Dashboard = ({ data }) => (
	<div className="content">
		<Widget styleName="widget--values">
			<DataIndex
				label="Транзакций"
				value={data.transactionsTotal}
				trend={data.transactionsTotalDelta}
				text="Совершено за отчетный период"
			/>
			<DataIndex
				label="Рублей"
				value={data.revenueTotal}
				trend={data.revenueTotalDelta}
				text="Общая выручка за отчётный период"
			/>
			<DataIndex
				label="%"
				value={data.transactionsNonCashPercentage}
				trend={data.transactionsNonCashPercentageDelta}
				text="Доля безналичных платежей"
			/>
			<DataIndex
				label="Рублей"
				value={data.averageCostTotal}
				trend={data.averageCostTotalDelta}
				text="Средняя стоимость одной поездки"
			/>
		</Widget>
		<Widget title="Доля поездок в разрезе способов оплаты" col={6}>
			<Chart
				chartType="PieChart"
				width="100%"
				data={[
					['Способ оплаты', 'Доля поездок'],
					...data.paymentMethods.map(method => ([
							method.name,
							method.transactionsPercentage
					])).sort((a, b) => b[1] - a[1])
				]}
				options={{
					backgroundColor: 'transparent',
					pieHole: 0.4,
					sliceVisibilityTreshold: 0.1,
					chartArea: {
						left: '10%',
						top: '10%',
						width: '80%',
						height: '80%'
					},
					legend: {
						alignment: 'center',
						position: 'right'
					},
					animation: {
						duration: 300,
						startup: true
					},
					pieResidueSliceLabel: 'Другие',
					colors: [
						'#6566BF',
						'#FAA916',
						'#69BFB8',
						'#F35D01',
						'#3D9970'
					]
				}}
			/>
		</Widget>
		<Widget title='Выручка, руб. Количество транзакций' col={6}>
			<Chart
				chartType="ComboChart"
				width="100%"
				data={[
					['Период', 'Количество транзакций', 'Выручка'],
					...data.entities.map(entity => (
						(!entity) ? false : [
							moment(entity.date).format('DD MMM'),
							entity.transactionsTotal,
							entity.revenueTotal
							]
					))
				]}
				options={{
					backgroundColor: 'transparent',
					chartArea: {
						left: '10%',
						top: '10%',
						width: '80%',
						height: '60%'
					},
					legend: {
						alignment: 'center',
						position: 'bottom'
					},
					colors: [
						'#6566BF',
						'#FAA916',
						'#69BFB8',
						'#F35D01',
						'#3D9970'
					],
					vAxes: {
						0: {
						},
						1: {
						}
					},
					vAxis: {
						format: 'short'
					},
					hAxis: {
						gridlines: {
							units: {
								days: { format: ['dd mm'] }
							}
						},
						format: 'dd MMM'
					},
					animation: {
						duration: 300,
						startup: true
					},
					series: {
						0: {
							type: 'bars',
							targetAxisIndex: 0
						},
						1: {
							type: 'line',
							targetAxisIndex: 1,
							pointsVisible: true,
							pointSize: 3
						}
					}
				}}
			/>
		</Widget>
		{!data.entities.length ? '' : <Widget title="Количество поездок в разрезе способов оплаты" col={6}>
			<Chart
				chartType="ComboChart"
				width="100%"
				data={[
					['Период', ...data.entities[0].paymentMethods.map(method => method.name)],
					...data.entities.map(entity => [
						moment(entity.date).format('DD MMM'), ...entity.paymentMethods.map(method => method.transactions)])
				]}
				options={{
					backgroundColor: 'transparent',
					chartArea: {
						left: '10%',
						top: '10%',
						width: '80%',
						height: '60%'
					},
					legend: {
						alignment: 'center',
						position: 'bottom'
					},
					colors: [
						'red',
						'#6566BF',
						'#FAA916',
						'#69BFB8',
						'#F35D01',
						'#3D9970'
					],
					vAxis: {
						format: 'short'
					},
					hAxis: {
						gridlines: {
							units: {
								days: { format: ['dd mm'] }
							}
						},
						format: 'dd MMM'
					},
					animation: {
						duration: 300,
						startup: true
					},
					seriesType: 'bars',
					isStacked: true
				}}
			/>
		</Widget>}
		{!data.entities.length ? '' : <Widget title="Выручка в разрезе способов оплаты" col={4}>
			<Chart
				chartType="ComboChart"
				width="100%"
				data={[
					['Период', ...data.entities[0].paymentMethods.map(method => method.name)],
					...data.entities.map(entity => [moment(entity.date).format('DD MMM'), ...entity.paymentMethods.map(method => method.revenue)])
				]}
				options={{
					backgroundColor: 'transparent',
					chartArea: {
						left: '10%',
						top: '10%',
						width: '80%',
						height: '60%'
					},
					legend: {
						alignment: 'center',
						position: 'bottom'
					},
					colors: [
						'red',
						'#6566BF',
						'#FAA916',
						'#69BFB8',
						'#F35D01',
						'#3D9970'
					],
					vAxis: {
						format: 'short'
					},
					hAxis: {
						gridlines: {
							units: {
								days: { format: ['dd mm'] }
							}
						},
						format: 'dd MMM'
					},
					animation: {
						duration: 300,
						startup: true
					},
					seriesType: 'bars',
					isStacked: true
				}}
			/>
		</Widget>}
		<Widget title="Доля безналичных платежей" col={4}>
			<Chart
				chartType="ComboChart"
				width="100%"
				data={[
					['Период', 'Безналичный', 'Наличный'],
					...data.entities.map(entity => {
						if (!entity) return false;
						return [moment(entity.date).format('DD MMM'), entity.transactionsNonCashPercentage, entity.transactionsCashPercentage];
					})
				]}
				options={{
					backgroundColor: 'transparent',
					chartArea: {
						left: '10%',
						top: '10%',
						width: '80%',
						height: '60%'
					},
					legend: {
						alignment: 'center',
						position: 'bottom'
					},
					colors: [
						'#6566BF',
						'#FAA916',
						'#69BFB8',
						'#F35D01',
						'#3D9970'
					],
					vAxis: {
						format: 'short'
					},
					hAxis: {
						format: 'dd MMM'
					},
					animation: {
						duration: 300,
						startup: true
					},
					seriesType: 'bars',
					isStacked: true,
					bars: 'vertical'
				}}
			/>
		</Widget>
		<Widget title="Средняя стоимость поездки в разрезе способов оплаты" col={4}>
			<Chart
				chartType="LineChart"
				width="100%"
				data={[
					['Период', 'Безналичный', 'Наличный', 'Все способы'],
					...data.entities.map(entity => {
						if (!entity) return false;
						return [
							moment(entity.date).format('DD MMM'),
							entity.averageCostCash,
							entity.averageCostNonCash,
							entity.averageCostTotal
						];
					})
				]}
				options={{
					backgroundColor: 'transparent',
					chartArea: {
						left: '10%',
						top: '10%',
						width: '80%',
						height: '60%'
					},
					legend: {
						alignment: 'center',
						position: 'bottom'
					},
					colors: [
						'#6566BF',
						'#FAA916',
						'#69BFB8',
						'#F35D01',
						'#3D9970'
					],
					vAxis: {
						format: 'short'
					},
					animation: {
						duration: 300,
						startup: true
					},
					hAxis: {
						gridlines: {
							units: {
								days: { format: ['dd mm'] }
							}
						},
						format: 'dd MMM'
					}
				}}
			/>
		</Widget>
	</div>
);

export default CSSModules(Dashboard, styles, { allowMultiple: true });
