import * as React from 'react';
import '../../styles/main';

const Page = ({ children }) =>
	<div className="page">
		{children}
	</div>
;

Page.propTypes = {
	children: React.PropTypes.node,
	title: React.PropTypes.string
};

export default Page;
