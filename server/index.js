const express = require('express');
const config  = require('./config');
const logger  = require('./utils/logger');
const path    = require('path');
const Entity    = require('./models/entity.js');
const Company   = require('./models/company.js');

const app =  express();

app.use(express.static( path.resolve(__dirname, '../dist/') ) );

app.get('/', (req, res) => res.sendFile(path.resolve(__dirname, '../dist/index.html')));

app.get('/entities', (req, res) => Entity.get(req.query).then( result => res.send(result) ).catch( err => res.send(err) ) );

app.get('/companies', (req, res) => Company.get(req.query).then( result => res.send(result) ).catch( err => res.send(err) ) );

app.listen(config.get('port'), () => {
	logger.info('Server listening on localhost:' + config.get('port'));
});
