'use strict';
const mongoose = require('../utils/mongoose');
const Schema = mongoose.Schema;
const logger = require('../utils/logger');

const CompanySchema = new Schema({
	date: { type: Date, default: Date.now },
	companies: [{
		label: String,
		routes: Number,
		terminals: Number,
		revenue: Number,
		transactions: Number,
		transactionsCash: Number,
		transactionsNoncash: Number
	}]
});

CompanySchema.statics.add = function (data) {
	let date = new Date(data[0]['Дата транзакции в системе'] + ' 00:00');
	return this.update(
		{ date: date },
		{
			$setOnInsert: {
				date: date,
				companies: data.map(row => ({
					label: row['Наименование предприятия'] || '',
					routes: +row['Количество маршрутов'] || 0,
					terminals: +row['Количество терминалов на линии'] || 0,
					revenue: +row['Выручка, руб'] || 0,
					transactions: +row['Количество транзакций'] || 0,
					transactionsCash: +row['Количество транзакций за наличные средства'] || 0,
					transactionsNoncash: +row['Количество транзакций по безналичному способу'] || 0
				}))
			}
		},
		{
			upsert: true
		}
	)
	.then(result => logger.info('Inserted ' + result.n + ' new companies entity'))
	.catch(err => logger.error(err))
	;
};

CompanySchema.statics.get = function(options) {
	const currentFromDate = new Date(+options.currentFromDate);
	const currentToDate = new Date(+options.currentToDate);

	return new Promise( (resolve, reject) => {
		this.aggregate([
				/**
				 * Get current period entities
				 */
				{ $match: { date: {
					$gt: currentFromDate,
					$lte: currentToDate
				} } },
				/**
				 * Write date to each company
				 */
				{ $unwind: '$companies' },
				/**
				 * Calculate company fields
				 */
				{ $group: {
					_id: '$companies.label',
					routes: { $max: '$companies.routes' },
					terminals: { $max: '$companies.terminals' },
					revenue: { $sum: '$companies.revenue' },
					transactions: { $sum: '$companies.transactions' },
					transactionsCash: { $sum: '$companies.transactionsCash' },
					transactionsNoncash: { $sum: '$companies.transactionsNoncash' },
					transactionsAvg: { $avg: '$companies.transactions' }
					// transactionsPerTerminal: { $divide: [ { $avg: '$companies.transactions' }, '$companies.terminals' ] }
				} },
				/**
				 * Delete '_id' field and add 'label' field
				 */
				{ $project: {
					_id: 0,
					label: '$_id',
					routes: 1,
					terminals: 1,
					revenue: 1,
					transactions: 1,
					transactionsCash: 1,
					transactionsNoncash: 1,
					transactionsAvg: 1
					// transactionsPerTerminal: 1
				} }
			])
			.exec( (err, result) => {
				if (err) reject(err);
				else resolve(result);
			});
	});
};

module.exports = mongoose.model('Company', CompanySchema);
