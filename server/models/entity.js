'use strict';
const mongoose = require('../utils/mongoose');
const logger = require('../utils/logger');
const config = require('../config/');
const round = require('mongo-round');
const decimals = config.get('decimals');

/**
 * Calculate percent
 * @param totalQuery
 * @param partQuery
 * @param {(Number|function)} decimals
 * @returns {Number}
 */
const toPercent = (totalQuery, partQuery, decimals) => round({$multiply: [{$divide: [100, totalQuery]}, partQuery]}, decimals);

const EntitySchema = new mongoose.Schema({
	date: { type: Date, default: Date.now },
	companyGroups: [{
		name: { type: String, default: '' },
		paymentMethods: [{
			name: { type: String, default: '' },
			isCash: { type: Boolean, default: false },
			transactions: { type: Number, default: 0 },
			revenue: Number
		}]
	}]
});

EntitySchema.statics.add = function(data) {
	const date = new Date(data[0]['Дата транзакции в системе'] + ' 00:00');

	let groups = {};

	data.forEach( row => {
		let id = row['Группа предприятий'];
		if (!groups[id]) groups[id] = { paymentMethods: [] };

		groups[id].name = id;
		groups[id].paymentMethods.push({
			name: row['Способ оплаты'],
			isCash: row['Способ оплаты'] === 'Наличные',
			transactions: +row['Количество транзакций'],
			revenue: +(row['Выручка, руб'].toString().replace(',', '.')) || 0
		});
	});

	let companyGroups = Object.keys(groups).map( key => groups[key]);

	return this.update(
		{ date: date },
		{ $setOnInsert: {
			date: date,
			companyGroups: companyGroups
		} },
		{ upsert: true }
	)
	.then( result => ( result.n && logger.info('Inserted new entity') ) )
	.catch( err => logger.error(err))
	;
};

EntitySchema.statics.get = function (request) {
	const currentFromDate = new Date(+request.currentFromDate);
	const currentToDate = new Date(+request.currentToDate);
	const prevFromDate = new Date(+request.prevFromDate);
	const prevToDate = new Date(+request.prevToDate);
	const groups = request.companyGroup ? request.companyGroup.split(',') : ['ГУП МО "МОСТРАНСАВТО"', 'Прочие перевозчики'];

	return new Promise( (resolve, reject) => {
		this.aggregate([
			// Get current period entities
			{$match: { date: { $gt: prevFromDate, $lte: currentToDate } }},

			{$unwind: '$companyGroups'},

			{$match : {'companyGroups.name': {$in: groups}}},

			{$unwind: '$companyGroups.paymentMethods'},

			{$group: {
				_id: {date: '$date', name: '$companyGroups.paymentMethods.name'},
				date: {$first: '$date'},
				name: {$first: '$companyGroups.paymentMethods.name'},
				groups: {$push: '$companyGroups.name'},
				transactions: {$sum: '$companyGroups.paymentMethods.transactions'},
				revenue: {$sum: '$companyGroups.paymentMethods.revenue'},
				isCash: {$first: '$companyGroups.paymentMethods.isCash'}
			}},

			{$sort: {name: 1}},

			{$group: {
				_id: '$date',
				date: {$first: '$date'},
				groups: {$addToSet: '$groups'},
				paymentMethods: {$push: {
					name: '$name',
					isCash: '$isCash',
					transactions: '$transactions',
					revenue: '$revenue',
					averageCost: {$divide: ['$revenue', '$transactions']}
				}},
				transactionsTotal: {$sum: '$transactions'},
				transactionsCash: {$sum: {$cond: ['$isCash', '$transactions', 0]}},
				transactionsNonCash: {$sum: {$cond: ['$isCash', 0, '$transactions']}},
				revenueTotal: {$sum: '$revenue'},
				revenueCash: {$sum: {$cond: ['$isCash', '$revenue', 0]}},
				revenueNonCash: {$sum: {$cond: ['$isCash', 0, '$revenue']}}
			}},

			{$project: {
				_id: 0,
				date: 1,
				groups: 1,
				paymentMethods: 1,
				transactionsTotal: round('$transactionsTotal', decimals),
				transactionsCash: round('$transactionsCash', decimals),
				transactionsCashPercentage: toPercent('$transactionsTotal', '$transactionsCash', decimals),
				transactionsNonCash: round('$transactionsNonCash', decimals),
				transactionsNonCashPercentage: toPercent('$transactionsTotal', '$transactionsNonCash', decimals),
				revenueTotal: round('$revenueTotal', decimals),
				revenueCash: round('$revenueCash', decimals),
				revenueCashPercentage: toPercent('$revenueTotal', '$revenueCash', decimals),
				revenueNonCash: round('$revenueNonCash', decimals),
				revenueNonCashPercentage: toPercent('$revenueTotal', '$revenueNonCash', decimals),
				averageCostTotal: round({$divide: ['$revenueTotal', '$transactionsTotal']}, decimals),
				averageCostCash: round({$divide: ['$revenueCash', '$transactionsCash']}, decimals),
				averageCostNonCash: round({$divide: ['$revenueNonCash', '$transactionsNonCash']}, decimals)
			}},

			{$sort: {date: 1}},

			{$group: {
				_id: 0,
				currentEntities: {$push: '$$CURRENT'},
				prevEntities: {$push: '$$CURRENT'}
			}},

			{$project: {
				_id: 0,
				'current.entities': {$filter: {
					input: '$currentEntities',
					as: 'entity',
					cond: {$and: [
						{$gt: ['$$entity.date', currentFromDate]},
						{$lte: ['$$entity.date', currentToDate]}
					]}
				}},
				'prev.entities': {$filter: {
					input: '$prevEntities',
					as: 'entity',
					cond: {$and: [
						{$gt: ['$$entity.date', prevFromDate]},
						{$lte: ['$$entity.date', prevToDate]}
					]}
				}}
			}},

			{$project: {
				_id: 0,
				current: {
					entities: '$current.entities',
					paymentMethods: '$current.entities.paymentMethods',
					transactionsTotal: round({$sum: '$current.entities.transactionsTotal'}, decimals),
					transactionsCash: round({$sum: '$current.entities.transactionsCash'}, decimals),
					transactionsCashPercentage: round({$avg: '$current.entities.transactionsCashPercentage'}, decimals),
					transactionsNonCash: round({$sum: '$current.entities.transactionsNonCash'}, decimals),
					transactionsNonCashPercentage: round({$avg: '$current.entities.transactionsNonCashPercentage'}, decimals),
					revenueTotal: round({$sum: '$current.entities.revenueTotal'}, decimals),
					revenueCash: round({$sum: '$current.entities.revenueCash'}, decimals),
					revenueCashPercentage: round({$avg: '$current.entities.revenueCashPercentage'}, decimals),
					revenueNonCash: round({$sum: '$current.entities.revenueNonCash'}, decimals),
					revenueNonCashPercentage: round({$avg: '$current.entities.revenueNonCashPercentage'}, decimals),
					averageCostTotal: round({$avg: '$current.entities.averageCostTotal'}, decimals),
					averageCostCash: round({$avg: '$current.entities.averageCostCash'}, decimals),
					averageCostNonCash: round({$avg: '$current.entities.averageCostNonCash'}, decimals)
				},
				prev: {
					transactionsTotal: round({$sum: '$prev.entities.transactionsTotal'}, decimals),
					transactionsCash: round({$sum: '$prev.entities.transactionsCash'}, decimals),
					transactionsCashPercentage: round({$avg: '$current.entities.transactionsCashPercentage'}, decimals),
					transactionsNonCash: round({$sum: '$prev.entities.transactionsNonCash'}, decimals),
					transactionsNonCashPercentage: round({$avg: '$current.entities.transactionsNonCashPercentage'}, decimals),
					revenueTotal: round({$sum: '$prev.entities.revenueTotal'}, decimals),
					revenueCash: round({$sum: '$prev.entities.revenueCash'}, decimals),
					revenueCashPercentage: round({$avg: '$current.entities.revenueCashPercentage'}, decimals),
					revenueNonCash: round({$sum: '$prev.entities.revenueNonCash'}, decimals),
					revenueNonCashPercentage: round({$avg: '$current.entities.revenueNonCashPercentage'}, decimals),
					averageCostTotal: round({$avg: '$prev.entities.averageCostTotal'}, decimals),
					averageCostCash: round({$avg: '$prev.entities.averageCostCash'}, decimals),
					averageCostNonCash: round({$avg: '$prev.entities.averageCostNonCash'}, decimals)
				}
			}},

			{$project: {
				_id: 0,
				entities: '$current.entities',
				paymentMethods: '$current.paymentMethods',
				transactionsTotal: round('$current.transactionsTotal', decimals),
				transactionsTotalDelta: round({$subtract: ['$current.transactionsTotal', '$prev.transactionsTotal']}, decimals),
				transactionsCash: round('$current.transactionsCash', decimals),
				transactionsCashDelta: round({$subtract: ['$current.transactionsCash', '$prev.transactionsCash']}, decimals),
				transactionsCashPercentage: round('$current.transactionsCashPercentage', decimals),
				transactionsCashPercentageDelta: round({$subtract: ['$current.transactionsCashPercentage', '$prev.transactionsCashPercentage']}, decimals),
				transactionsNonCash: round('$current.transactionsNonCash', decimals),
				transactionsNonCashDelta: round({$subtract: ['$current.transactionsNonCash', '$prev.transactionsNonCash']}, decimals),
				transactionsNonCashPercentage: round('$current.transactionsNonCashPercentage', decimals),
				transactionsNonCashPercentageDelta: round({$subtract: ['$current.transactionsNonCashPercentage', '$prev.transactionsNonCashPercentage']}, decimals),
				revenueTotal: round('$current.revenueTotal', decimals),
				revenueTotalDelta: round({$subtract: ['$current.revenueTotal', '$prev.revenueTotal']}, decimals),
				revenueCash: round('$current.revenueCash', decimals),
				revenueCashDelta: round({$subtract: ['$current.revenueCash', '$prev.revenueCash']}, decimals),
				revenueCashPercentage: round('$current.revenueCashPercentage', decimals),
				revenueCashPercentageDelta: round({$subtract: ['$current.revenueCashPercentage', '$prev.revenueCashPercentage']}, decimals),
				revenueNonCash: round('$current.revenueNonCash', decimals),
				revenueNonCashDelta: round({$subtract: ['$current.revenueNonCash', '$prev.revenueNonCash']}, decimals),
				revenueNonCashPercentage: round('$current.revenueNonCashPercentage', decimals),
				revenueNonCashPercentageDelta: round({$subtract: ['$current.revenueNonCashPercentage', '$prev.revenueNonCashPercentage']}, decimals),
				averageCostTotal: round('$current.averageCostTotal', decimals),
				averageCostTotalDelta: round({$subtract: ['$current.averageCostTotal', '$prev.averageCostTotal']}, decimals),
				averageCostCash: round('$current.averageCostCash', decimals),
				averageCostCashDelta: round({$subtract: ['$current.averageCostCash', '$prev.averageCostCash']}, decimals),
				averageCostNonCash: round('$current.averageCostNonCash', decimals),
				averageCostNonCashDelta: round({$subtract: ['$current.averageCostNonCash', '$prev.averageCostNonCash']}, decimals)
			}},

			{$unwind: '$paymentMethods'},

			{$unwind: '$paymentMethods'},

			{$sort: {'paymentMethods.name': 1}},

			{$group: {
				_id: '$paymentMethods.name',
				methodName: {$first: '$paymentMethods.name'},
				methodTransactions: {$sum: '$paymentMethods.transactions'},
				methodRevenue: {$sum: '$paymentMethods.revenue'},
				methodIsCash: {$first: '$paymentMethods.isCash'},
				entities: {$first: '$entities'},
				transactionsTotal: {$first: '$transactionsTotal'},
				transactionsTotalDelta: {$first: '$transactionsTotalDelta'},
				transactionsCash: {$first: '$transactionsCash'},
				transactionsCashDelta: {$first: '$transactionsCashDelta'},
				transactionsCashPercentage: {$first: '$transactionsCashPercentage'},
				transactionsCashPercentageDelta: {$first: '$transactionsCashPercentageDelta'},
				transactionsNonCash: {$first: '$transactionsNonCash'},
				transactionsNonCashDelta: {$first: '$transactionsNonCashDelta'},
				transactionsNonCashPercentage: {$first: '$transactionsNonCashPercentage'},
				transactionsNonCashPercentageDelta: {$first: '$transactionsNonCashPercentageDelta'},
				revenueTotal: {$first: '$revenueTotal'},
				revenueTotalDelta: {$first: '$revenueTotalDelta'},
				revenueCash: {$first: '$revenueCash'},
				revenueCashDelta: {$first: '$revenueCashDelta'},
				revenueCashPercentage: {$first: '$revenueCashPercentage'},
				revenueCashPercentageDelta: {$first: '$revenueCashPercentageDelta'},
				revenueNonCash: {$first: '$revenueNonCash'},
				revenueNonCashDelta: {$first: '$revenueNonCashDelta'},
				revenueNonCashPercentage: {$first: '$revenueNonCashPercentage'},
				revenueNonCashPercentageDelta: {$first: '$revenueNonCashPercentageDelta'},
				averageCostTotal: {$first: '$averageCostTotal'},
				averageCostTotalDelta: {$first: '$averageCostTotalDelta'},
				averageCostCash: {$first: '$averageCostCash'},
				averageCostCashDelta: {$first: '$averageCostCashDelta'},
				averageCostNonCash: {$first: '$averageCostNonCash'},
				averageCostNonCashDelta: {$first: '$averageCostNonCashDelta'}
			}},

			{$group: {
				_id: 0,
				paymentMethods: {$push: {
					name: '$methodName',
					transactions: '$methodTransactions',
					isCash: '$methodIsCash',
					revenue: '$methodRevenue',
					transactionsPercentage: toPercent('$transactionsTotal', '$methodTransactions', decimals),
					revenuePercentage: toPercent('$revenueTotal', '$methodRevenue', decimals),
					averageCost: round({$divide: ['$methodRevenue', '$methodTransactions']}, decimals)
				}},
				entities: {$first: '$entities'},
				transactionsTotal: {$first: '$transactionsTotal'},
				transactionsTotalDelta: {$first: '$transactionsTotalDelta'},
				transactionsCash: {$first: '$transactionsCash'},
				transactionsCashDelta: {$first: '$transactionsCashDelta'},
				transactionsCashPercentage: {$first: '$transactionsCashPercentage'},
				transactionsCashPercentageDelta: {$first: '$transactionsCashPercentageDelta'},
				transactionsNonCash: {$first: '$transactionsNonCash'},
				transactionsNonCashDelta: {$first: '$transactionsNonCashDelta'},
				transactionsNonCashPercentage: {$first: '$transactionsNonCashPercentage'},
				transactionsNonCashPercentageDelta: {$first: '$transactionsNonCashPercentageDelta'},
				revenueTotal: {$first: '$revenueTotal'},
				revenueTotalDelta: {$first: '$revenueTotalDelta'},
				revenueCash: {$first: '$revenueCash'},
				revenueCashDelta: {$first: '$revenueCashDelta'},
				revenueCashPercentage: {$first: '$revenueCashPercentage'},
				revenueCashPercentageDelta: {$first: '$revenueCashPercentageDelta'},
				revenueNonCash: {$first: '$revenueNonCash'},
				revenueNonCashDelta: {$first: '$revenueNonCashDelta'},
				revenueNonCashPercentage: {$first: '$revenueNonCashPercentage'},
				revenueNonCashPercentageDelta: {$first: '$revenueNonCashPercentageDelta'},
				averageCostTotal: {$first: '$averageCostTotal'},
				averageCostTotalDelta: {$first: '$averageCostTotalDelta'},
				averageCostCash: {$first: '$averageCostCash'},
				averageCostCashDelta: {$first: '$averageCostCashDelta'},
				averageCostNonCash: {$first: '$averageCostNonCash'},
				averageCostNonCashDelta: {$first: '$averageCostNonCashDelta'}
			}}
		])
		.exec( (err, result) => {
			if (err) reject(err);
			else resolve(result);
		});
	});
};

module.exports = mongoose.model('Entity', EntitySchema);
