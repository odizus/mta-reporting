'use strict';
const appRoot    = require('app-root-path');
const Converter  = require('csvtojson').Converter;
const imap       = require('imap-simple');
const config     = require(appRoot + '/server/config');
const Entity     = require(appRoot + '/server/models/entity.js');
const Company    = require(appRoot + '/server/models/company.js');
const moment     = require('moment');
const logger     = require(appRoot + '/server/utils/logger');


const parseMail = () => {
	const connection = imap.connect({imap: config.get('mail:0')});

	connection.then(connection => {
		logger.info( 'Connected to email ' + config.get('mail:0:user') );

		connection.openBox('inbox')
		.then(function() {
			const searchCriteria = ['UNSEEN'];
			const fetchOptions = {
				bodies: ['HEADER'],
				struct: true,
				// markSeen: true
			};

			return connection.search(searchCriteria, fetchOptions);
		})
		.then(messages => {
			if (!messages.length) {
				logger.info( 'No new messages on ' + config.get('mail:0:user') );
				return false;
			}

			let attachments = [];

			messages.forEach(message => {
				let parts = imap.getParts(message.attributes.struct);
				attachments = attachments.concat(parts.filter(part => {
					return part.disposition && (part.disposition.type === 'attachment' || part.disposition.type === 'ATTACHMENT');
				}).map(function (part) {
					// retrieve the attachments only of the messages with attachments
					return connection.getPartData(message, part)
						.then(function (partData) {
							return {
								filename: part.disposition.params.filename,
								data: partData
							};
						});
				}));
			});

			return Promise.all(attachments);
		})
		.then(attachments => {
			if (!attachments) return false;
			if (!attachments.length) {
				logger.info('No attachments in message');
				return false;
			}

			attachments.forEach(attachment => {
				let filename = new Buffer(attachment.filename.substring(10), 'base64').toString('utf8');
				let dataCSV = (new Buffer(attachment.data).toString('utf16le')).toString('utf8');

				// Converting CSV data to JSON
				new Converter({
					delimiter: '\t',
					toArrayString: true
				}).fromString(dataCSV, (err, data) => {
					if (err) throw err;
					
					let isTransactions = new RegExp( config.get('attachmentNameRegExp:transactions') ).test(filename);
					let isCompany = new RegExp( config.get('attachmentNameRegExp:company') ).test(filename);

					if (isTransactions) Entity.add(data);
					else if (isCompany) Company.add(data);
					else return false;
				});
			});
		});
	});
};


module.exports = parseMail;
