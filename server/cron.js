'use strict';
const appRoot    = require('app-root-path');
const config     = require(appRoot + '/server/config');
const logger     = require(appRoot + '/server/utils/logger');
const CronJob    = require('cron').CronJob;
const parseMail  = require('./mail');

const mailParsingJob = new CronJob(
	'00 * * * * *',
	() => {
		logger.info('Start mail parsing');
		parseMail();
	},
	() => {
		logger.info('Mail parsing is stopped');
	},
	true, /* Start the job right now */
	config.get('timezone') /* TimeZone */
);

mailParsingJob.start();

logger.info( 'Cron started at ' + new Date().toLocaleString() );
