var path = require('path');
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
	devtool: 'source-map',
	entry: './src/index',
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'bundle.js'
	},
	resolve: {
		alias: {
			components: path.resolve(__dirname, './src/components'),
			containers: path.resolve(__dirname, './src/containers'),
			utils: path.resolve(__dirname, './src/utils'),
			actions: path.resolve(__dirname, './src/actions'),
			tepdesk: path.resolve(__dirname, './src/components/tepdesk')
		},
		extensions: ['', '.js', '.jsx', '.scss']
	},
	plugins: [
		// new webpack.HotModuleReplacementPlugin(),
		new ExtractTextPlugin('style.css', { allChunks: true }),
		new webpack.optimize.DedupePlugin(),
		new HtmlWebpackPlugin({
			template: path.resolve(__dirname, 'src/index.html')
		})
	],
	module: {
	loaders: [
		{
			test: /\.(js|jsx)$/,
			exclude: /(node_modules|bower_components|server)/,
			loader: 'babel',
			query: {
				cacheDirectory: true,
				plugins: [
					'transform-decorators-legacy',
					'transform-class-properties'
				],
				presets: ['es2015', 'react', 'stage-2']
			},
			include: path.join(__dirname, 'src')
		},
		// {
		//     test: /\.scss$/,
		//     include: /src\/components\//,
		//     loader: ExtractTextPlugin.extract([
		//         'css?sourceMap&modules&importLoaders=1&localIdentName=[local]_[hash:base64:5]',
		//         'postcss',
		//         'sass?sourceMap'
		//     ].join('!'))
		// },
		{
			test: /\.scss$/,
			exclude: /src\/components\//,
			loader: ExtractTextPlugin.extract('css?sourceMap!postcss!sass?sourceMap')
		},
		{
		    test: /\.scss$/,
			include: /src\/components\//,
		    loaders: [
		        'style?sourceMap',
		        'css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
		        'sass?sourceMap'
		    ]
		},
		{
			test: /\.(png|jpg)$/,
			loader: 'url?limit=25000'
		},
		{
			test: /\.(woff|woff2)$/,
			loader: 'url?limit=10000&name=[path][name]_[hash:base64:5].[ext]'
		},
		{
			test: /\.svg$/,
			loader: 'svg-inline'
		}
	]
	},
	postcss: function() {
		return [
			autoprefixer({ browsers: 'last 2 versions' })
		];
	},
	sassLoader: {
		includePaths: [
			path.resolve(__dirname, './src/styles')
		]
	}
};
